import logo from './logo.svg';
import './App.css';
import Board from './components/Board';
import './index.css'



function App() {
  return (
    <div className="App">
      <h1 className='header'> TIC TAC TOE</h1>
      <Board />
    </div>
  );
}

export default App;

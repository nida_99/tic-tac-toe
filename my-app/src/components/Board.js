import React, { useState } from 'react';
import Square from './Square';
import '../index.css';

function TicTacToe() {

    const [grid, setGrid] = useState(Array(9).fill(null));
    const [player, setPlayer] = useState(true);
    const [winner, setWinner] = useState("");
    const [gameFinished, setGameFinished] = useState(false);


    const Combination = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6],
    ];

    function handleClick(i) {
        if (gameFinished) {
            return;
        }
        if (grid[i] === null) {
            const newGrid = [...grid]
            newGrid[i] = player ? "X" : "O"
            setGrid(newGrid);
            setPlayer(!player)
        }

    }

    function isGameOver() {
        if (!gameFinished) {
            for (let index = 0; index < Combination.length; index++) {
                const [a, b, c] = Combination[index];

                if (grid[a] && grid[a] === grid[b] && grid[b] === grid[c]) {

                    setWinner(grid[a]);
                    setGameFinished(true);
                    return grid[a];
                }
                if (!grid.includes(null)) {
                    setGameFinished(true)

                };
            };

        };
    };

    isGameOver()

    const gameWinner = isGameOver(grid);
    let gameStatus;
    if (gameWinner !== "") {
        gameStatus = 'Player turn : ' + (player ? "X" : "O")
    }
    else {
        gameStatus = 'Player turn : '
    }

    function renderSquare(i) {
        return (
            <Square value={grid[i]} onClick={() => handleClick(i)} />
        );
    };

    function restartGame() {
        setGrid(Array(9).fill(null));
        setWinner("")
        setGameFinished(false)
        setPlayer(true);

    };
    return (
        <div className='board'>
            <div className='row'>
                {renderSquare(0)}
                {renderSquare(1)}
                {renderSquare(2)}
            </div>
            <div className='row'>
                {renderSquare(3)}
                {renderSquare(4)}
                {renderSquare(5)}
            </div>
            <div className='row'>
                {renderSquare(6)}
                {renderSquare(7)}
                {renderSquare(8)}
            </div>
            <section className='status'>
                <h5> {gameStatus}</h5>
                <h5 >Winner Player : {winner}</h5>
            </section>
            <button className="restart-btn" onClick={() => restartGame()}>Restart Game</button>
        </div>
    )
}

export default TicTacToe;